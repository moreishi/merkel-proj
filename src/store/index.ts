import { createStore } from 'vuex'
import axios from "axios";

export default createStore({
  state: {
    hackerNewsArticlesAll: [],
    hackerNewsArticles: [],
    hackerNewsArticle: {},
    hackerNewsArticleImg: null,
    page: 0,
    offset: 0,
    max: 0
  },
  getters: {
      getCurrentPage: (state) => {
          return state.page;
      },
      getOffset: (state) => {
          return state.offset;
      },
      getMax: (state) => {
          return state.max;
      },
      getHackerNewsArticles: (state) => {
          return state.hackerNewsArticles;
      },
      getHackerNewsArticle: (state) => {
          return state.hackerNewsArticle;
      },
      getHackerNewsArticleImg: (state) => {
          return state.hackerNewsArticleImg;
      }
  },
  mutations: {
      setPagesOffset: (state, {page, offset}) => {
          state.page += page;
          state.offset += offset
      },
      setHackerNewsArticle: (state, data) => {
          state.hackerNewsArticle = data;
      },
      setHackerNewsArticleImg: (state, data) => {
          state.hackerNewsArticleImg = data;
      },
      setHackerNewsArticles: (state:any, data:any) => {
          state.hackerNewsArticles.push(data);
      },
      setHackerNewsArticlesAll: (state, data) => {
          state.hackerNewsArticlesAll = data;
      }
  },
  actions: {
      getHackerNewsArticlesAll: async (store) => {
          const {data: articles} = await axios.get("https://hacker-news.firebaseio.com/v0/topstories.json");
          store.commit('setHackerNewsArticlesAll', articles);
      },
      getHackerNewsArticles: async (store) => {
        const {data: articles} = await axios.get("https://hacker-news.firebaseio.com/v0/topstories.json");
        store.commit('setHackerNewsArticles', articles);
      },
      getHackerStory: async (store, param) => {
        const {data: article} = await axios.get(`https://hacker-news.firebaseio.com/v0/item/${param}.json`);
        store.commit('setHackerNewsArticle', article);
      },
      getHackerMoreStories: async (store, { page, offset }) => {

          const currentPage = store.state.page + page;
          const currentOffset = offset;

          store.commit('setPagesOffset', {page, offset});
          const stories = store.state.hackerNewsArticlesAll.slice((currentPage - 1) * currentOffset, currentPage * currentOffset);

          stories.forEach(async (v, i) => {
              const {data} = await axios.get(`https://hacker-news.firebaseio.com/v0/item/${v}.json`);
              store.commit('setHackerNewsArticles', data);
          });
      }
  },
  modules: {

  }
})
